# Web Application with nextJs

## Features

```
> Typescript
> Css in Js - Emotion Js
> Prettier
> Eslint
> Custom routes
> Jest X react-testing-library
> Husky
> Deploy with now
```

## Coming soon

```
> Protect Master branch
```

## Nota Bene

```
> Build failed if test in pages
```

## Commandes

```
> docker build -t nextjs .
> docker container ls
> docker run -d -p 3333:3000 nextjs:latest
```
