import React, { ReactNode } from 'react';
import Head from 'next/head';
import { css, Global } from '@emotion/core';
import { COLOR } from '../../styles';

interface IContainer {
  children: ReactNode;
  title: string;
}

export const Container: React.FC<IContainer> = ({ children, title }) => {
  return (
    <>
      <Global
        styles={css`
          html,
          body {
            margin: 0;
            background: ${COLOR.BLACK};
            color: ${COLOR.BLACK};
            box-sizing: border-box;
            font-family: Helvetica, Arial, sans-serif;
          }
        `}
      />
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {children}
    </>
  );
};
