import React from 'react';
import { render } from '@testing-library/react';
import { Container } from '../Container';

describe('<Container />', () => {
  test('it should render children', () => {
    const { container } = render(
      <Container title="Page title">
        <div>children</div>
      </Container>
    );
    expect(container).toMatchSnapshot();
  });
});
