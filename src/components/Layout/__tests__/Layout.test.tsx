import React from 'react';
import { render } from '@testing-library/react';
import { Layout } from '../Layout';

describe('<Layout />', () => {
  test('it should render children', () => {
    const { container } = render(
      <Layout>
        <div>children</div>
      </Layout>
    );
    expect(container).toMatchSnapshot();
  });
});
