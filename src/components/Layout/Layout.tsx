/** @jsx jsx */
import { jsx } from '@emotion/core';

interface ILayout {
  children: React.ReactNode;
}

export const Layout: React.FC<ILayout> = ({ children }) => {
  return (
    <div
      css={{
        width: '980px',
        margin: 'auto'
      }}
    >
      {children}
    </div>
  );
};
