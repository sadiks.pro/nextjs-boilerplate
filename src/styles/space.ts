export const spaces = {
  XXSMALL: 5,
  XSMALL: 10,
  SMALL: 16,
  MEDIUM: 20,
  LARGE: 26,
  XLARGE: 30,
  XXLARGE: 40
};
