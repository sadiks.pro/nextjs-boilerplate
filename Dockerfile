# base image
FROM mhart/alpine-node

# set working directory
WORKDIR /app

COPY . .

RUN npm install
RUN npm run build

EXPOSE 3000
CMD ["npm", "run", "start"]
