import React from 'react';
import Link from 'next/link';
import { Container } from '../src/components/Container/Container';
import { Layout } from '../src/components/Layout/Layout';

const Other: React.FC = () => {
  return (
    <Container title="Other page">
      <Layout>
        <div>Other page</div>
        <Link href="/">
          <a href="/">Home</a>
        </Link>
      </Layout>
    </Container>
  );
};

export default Other;
