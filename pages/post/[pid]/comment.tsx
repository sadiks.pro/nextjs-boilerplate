import React from 'react';
import Link from 'next/link';

const Comment: React.FC = () => {
  return (
    <p>
      Comment list
      <Link href="/">
        <button type="button">home</button>
      </Link>
    </p>
  );
};

export default Comment;
