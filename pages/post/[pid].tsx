import React from 'react';
import { useRouter } from 'next/router';

const Post: React.FC = () => {
  const router = useRouter();
  const { pid } = router.query;

  return <p>Post: {pid}</p>;
};

export default Post;
