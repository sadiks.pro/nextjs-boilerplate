/** @jsx jsx */
import { jsx } from '@emotion/core';
import Link from 'next/link';
import { COLOR } from '../src/styles';
import { Container } from '../src/components/Container/Container';
import { Layout } from '../src/components/Layout/Layout';

const Home: React.FC = () => {
  return (
    <Container title="Homepage">
      <Layout>
        <div
          css={{
            padding: '32px',
            backgroundColor: 'aquamarine',
            fontSize: '24px',
            borderRadius: '4px',
            ':hover': {
              color: COLOR.WHITE
            }
          }}
        >
          Welcome to Next.js!
          <Link href="other">
            <a href="other">go other</a>
          </Link>
          <Link href="/post/espresso">
            <a href="/post/espresso">espresso product</a>
          </Link>
        </div>
      </Layout>
    </Container>
  );
};

export default Home;
